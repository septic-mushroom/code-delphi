unit UField;

interface

uses
  SysUtils, Types, UCoord, UCell;

type

  // ����� ����� ���������� �� ����� ������ ������ ��� �� ������ "���������"

  // ����.
  TField = class
  private
    FWidth: Integer;         // ������ ���� � �������
    FHeight: Integer;        // ������ ���� � �������
    FArea: array of array of TCell;  // ������ ������ ����
  private
    // ����� ������� ��� �������� Cell.
    function GetCell(X, Y: Integer): TCell;  overload;
    function GetCell(coord: TCoord): TCell;  overload;
    // ����������� ������ ����� Data ([0.0 .. 1.0]).
    procedure NormalizeArray(var Data: array of Single);
    // ��������� � ������ Data ����� �� ����� FilePath. � ��������� Size ������������ ����������� �������.
    function LoadArrayFromFile(const FilePath: String; var Size: Integer; var Data: TSingleDynArray): Boolean;
  public
    // �����������.
    constructor Create(Width, Height: Integer);
    // ����������.
    destructor Destroy; override;
    // ���������� ����������������� ����������.
    function Tor(X, Y: Integer; isEnabled: boolean): TCoord; overload;
    function Tor(const C: TCoord; isEnabled: boolean): TCoord; overload;
    // ���������� True, ���� ���������� ��������� � �������� ����.
    function IsInside(X, Y: Integer): Boolean;  overload;
    function IsInside(coord: TCoord): Boolean;  overload;
    // ������� ���� � ��������� ������, ���� �� ��� ����.
    procedure Kill(X, Y, step: Integer);
    // ���������� ������� (� ��������� ������) �� ������� ����� �� ��������� ����� ��������.
    procedure Humus(X, Y: Integer);
    // �������� ����� �������� �� ���������� �����. ���� ��� �������� ����� ��������� ������,
    // ������� ������ False. �������� Normalize ����������, ����� �� �������������� ��������.
    function LoadResourcesFromFile(const FilePath: String; Normalize: Boolean): Boolean;
  public
    // ������ � ������ ���� � �������.
    property Width: Integer read FWidth;
    property Height: Integer read FHeight;
    // ������ ���� (������� X, Y ������ ���������� �� 0).
    property Cells[X, Y: Integer]: TCell read GetCell; default;
    function isExists(coord: TCoord):boolean;
    function hasDifferentNode(baseCell, checkedCell: TCoord):boolean;
    function getSource(X, Y: integer): Single; overload;
    function getSource(coord: TCoord): Single; overload;
    function getFamilyId(coord: TCoord): integer; overload;
    function getFamilyId(X, Y: integer): integer; overload;

    function getPreviousDirection(coord: TCoord):TDirection;  overload;  //����������� ����� ����� �� ���������� ����
    function getPreviousDirection(X, Y: integer):TDirection;  overload;
  end;

implementation

//----------------------------------------------------------------------------------------------------------------------
constructor TField.Create(Width, Height: Integer);
var
  i, j: Integer;
begin
  // ���������� ������ � ������ ����.
  FWidth := Width; FHeight := Height;
  // ������������ �������� ������ � ������.
  if (FWidth <= 0) or (FHeight <= 0) then begin
    FWidth := 0;
    FHeight := 0;
  end;
  // ������ ������ ������� FCells.
  SetLength(FArea, FWidth);
  // ������� ������� ������.
  for i := 0 to FWidth - 1 do begin
    SetLength(FArea[i], FHeight);    //��� ������������, ������ ���������� ������
    for j := 0 to FHeight - 1 do begin
      FArea[i, j] := TCell.Create;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
destructor TField.Destroy;
var
  i, j: Integer;
begin
  // ���������� ������� ������.
  for i := 0 to Length(FArea)-1 do begin
    for j := 0 to Length(FArea[i])-1 do begin
      FreeAndNil(FArea[i, j]);
    end;
    SetLength(FArea[i], 0);
  end;
  SetLength(FArea, 0);
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.GetCell(X, Y: Integer): TCell;
begin
  // ��������� ������������ ���������.
  if not IsInside(X, Y) then
    raise Exception.Create('������������ ���������� ������' + IntToStr(X)+':'+IntToStr(Y));
  // ���������� ������ ������.
  Result := FArea[X, Y];
end;

function TField.GetCell(coord: TCoord): TCell;
begin
   Result := GetCell(coord.X, coord.Y);
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.IsInside(X, Y: Integer): Boolean;
begin
  Result := (X >= 0) and (X < FWidth) and (Y >= 0) and (Y < FHeight);
end;

function TField.IsInside(coord: TCoord): Boolean;
begin
  Result := IsInside(coord.X, coord.Y);
end;
//----------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------
//
//            ��� Kill ������� ��� ������� ������? ���� �������, �� ���� �������������
//
//-------------------------------------------------------------------------------------------------------
procedure TField.Kill(X, Y, step: Integer);
var
  Cell: TCell;
begin
  // �������� ������ ������.
  Cell := GetCell(X, Y);
  Cell.Kill(step);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TField.Humus(X, Y: Integer);
var
  Cell: TCell;
begin
  // �������� ������ ������.
  Cell := GetCell(X, Y);
  Cell.toHumus;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.LoadArrayFromFile(const FilePath: String; var Size: Integer; var Data: TSingleDynArray): Boolean;
var
  F: TextFile;
  X, Y, K: Integer;
  FS: TFormatSettings;
  Delim, DS: Char;
  S, S1: String;
begin
  Data := nil;
  Result := False;
  // ���������� ��������� ����������� ������� �����.
  GetLocaleFormatSettings(0, FS);
  DS := FS.DecimalSeparator;
  try
    // ��������� ����.
    AssignFile(F, FilePath);
    Reset(F);
    try
      // ������ ����� ������ ����� ����� - ����������� ����.
      Readln(F, Size);
      // ��������� ��������.
      if Size <= 0 then Exit;
      // �������� ������ ��� ������.
      SetLength(Data, Size * Size);
      // ��������� ��������� ������ �� �����.
      for Y := 0 to Size - 1 do begin
        // ������ ������.
        Readln(F, S);
        // ���������� ����������� �����.
        if Pos(';', S) > 0 then Delim := ';' else Delim := ',';
        // ��������� ������: � ��� ������ ���� Size �����, ����������� Delim.
        for X := 0 to Size - 1 do begin
          // ������� ������� ��������� �������.
          K := Pos(Delim, S);
          // ���� ����������� ���, ����� ��� ������.
          if K = 0 then K := Length(S) + 1;
          // �������� ����������� ������� �����.
          S1 := StringReplace(Copy(S, 1, K - 1), ',', DS, [rfReplaceAll]);
          S1 := StringReplace(S1, '.', DS, [rfReplaceAll]);
          // ��������� ������ � ����� � ��������� � Data.
          Data[X + Y * Size] := StrToFloat(S1);
          // ������� ����������� ����� �� ������.
          S := Copy(S, K + 1, MaxInt);
        end;
      end;
      // ������ ������� ���������.
      Result := True;
    finally
      // ��������� ����.
      CloseFile(F);
    end;
  except
    // ������ ���������� ������. ������� ������ False.
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.LoadResourcesFromFile(const FilePath: String; Normalize: Boolean): Boolean;
var
  Data: TSingleDynArray;
  Size, X, Y: Integer;
begin
  // ��������� ������ ����� �� �����.
  Result := LoadArrayFromFile(FilePath, Size, Data);
  // ����������� ������ � ������ ������.
  if not Result then Exit;
  // ����������� ��������, ���� �����.
  if Normalize then NormalizeArray(Data);
  // �������� ������� �������� �� ����.
  for Y := 0 to FHeight - 1 do begin
    for X := 0 to FWidth - 1 do begin
            //TModel.FField[x,y].SourceIn:=0.0;
      if (X < Size) and (Y < Size) then
        FArea[X, Y].Source := Data[X + Y * Size]
      else
        FArea[X, Y].Source := 0.0;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TField.NormalizeArray(var Data: array of Single);
var
  RMin, RMax: Single;
  i: Integer;
begin
  if Length(Data) = 0 then Exit;
  // ������� ����. � ���. ��������.
  RMin := Data[0];
  RMax := RMin;
  for i := 1 to High(Data) do begin
    if Data[i] < RMin then begin
      RMin := Data[i];
    end;
    if Data[i] > RMax then begin
      RMax := Data[i];
    end;
  end;
  // ������ ����������� ��������.
  for I := 0 to High(Data) do begin
    if RMax - RMin > 0 then
      Data[i] := (Data[i] - RMin) / (RMax - RMin)
    else
      Data[i] := 0.5;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.Tor(X, Y: Integer; isEnabled: boolean): TCoord;
begin
  // �������� ����������.
  if isEnabled then begin
    Result.X := (X + FWidth) mod FWidth;
    Result.Y := (Y + FHeight) mod FHeight;
  end else begin
    Result.X := X;
    Result.Y := Y;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TField.Tor(const C: TCoord; isEnabled: boolean): TCoord;
begin
  Result := Tor(C.X, C.Y, isEnabled);
end;

function TField.isExists(coord: TCoord):boolean;
begin
  Result := GetCell(coord).Exists;
end;

function TField.hasDifferentNode(baseCell, checkedCell: TCoord):boolean;
begin
  if not isExists(checkedCell) then begin
    Result := false;
    exit;
  end;

  Result := GetCell(baseCell).Node <> GetCell(checkedCell).Node;
  Result := true;  //�� ���� �������. ��������� ��������� �� ��������
end;

function TField.getSource(X, Y: integer): Single;
begin
  Result := GetCell(X, Y).Source;
end;

function TField.getSource(coord: TCoord): Single;
begin
  Result := GetSource(coord.X, coord.Y);
end;

function TField.getPreviousDirection(coord: TCoord):TDirection; //����������� ����� ����� �� ���������� ����
var
  cell: TCell;
begin
  result := dNone;

  if not isExists(coord) then begin
    exit;
  end;

  cell := GetCell(coord);

  if not isInside(cell.RelatedCellCoord) then begin
    exit;
  end;

  result := GetCell(cell.RelatedCellCoord).Direction;
end;

function TField.getPreviousDirection(X, Y: integer):TDirection;
begin
   result := getPreviousDirection(Coord(X, Y));
end;

function TField.getFamilyId(coord: TCoord): integer;
begin
  result := GetCell(coord).FamilyId;
end;

function TField.getFamilyId(X, Y: integer): integer;
begin
  result := getFamilyId(Coord(X, Y));
end;

end.
