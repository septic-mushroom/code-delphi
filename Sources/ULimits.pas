unit ULimits;

interface

type
  // ������� ��������.
  TLimits = record
    StarvationDeath: Single;  // �������� �������� � ������, ���� �������� ��������� �������� ������
    OvereatingToxic: Single;  // �������� �������� � ������, ���� �������� ���� ����������
    FullOvereatingToxic: Single;
    GrowthConsumptionLimits: Single; //������� ������� �� ����� �� ���
    LivingConsumptionLimit: Single; // ������� ������� �� ���� �� ���
    VelocityMean, VelocityMaxRes: Single;
  public
    // ��������� ����� ������. ���� �������� ���������, ������� ��������
    // �� � ������ True. ���� �������� �����������, ������� ������ False.
    function Velocity(const VelocityMean, VelocityMaxRes: String): Boolean;
    function SetLimits(const StarvationDeath, OvereatingToxic: String): Boolean;
    function SetConsumption(const GrowthConsumptionLimits, LivingConsumptionLimit: String): Boolean;
  end;

implementation

//----------------------------------------------------------------------------------------------------------------------
function TLimits.SetLimits(const StarvationDeath, OvereatingToxic: String): Boolean;
var
  StarvationValue, OvereatingValue: Single;
  E: Integer;
begin
  Result := False;
  // ������������ �������� Starvation.
  Val(StarvationDeath, StarvationValue, E);
  // ��������� ������������ �������� Starvation.
  if (E <> 0) or (StarvationValue < 0.0) then Exit;
  // ������������ �������� Overeating.
  Val(OvereatingToxic, OvereatingValue, E);
  // ��������� ������������ �������� Overeating.
  if (E <> 0) or (OvereatingValue < 0.0) or (OvereatingValue <= StarvationValue) then Exit;
  FullOvereatingToxic := 2*Self.OvereatingToxic;
  // ��������� ��� ����� �������� ������.
  Self.StarvationDeath := StarvationValue;
  Self.OvereatingToxic := OvereatingValue;
  // ��� �������.
  Result := True;
end;

function TLimits.Velocity(const VelocityMean, VelocityMaxRes: String): Boolean;
var
  VelocityMeanValue, VelocityMaxResValue: Single;
  E: Integer;
begin
  Result := False;
  // ������������ �������� Starvation.
  Val(VelocityMean, VelocityMeanValue, E);
  // ��������� ������������ �������� Starvation.
  if (E <> 0) or (VelocityMeanValue < 0.0) then Exit;
  // ������������ �������� Overeating.
  Val(VelocityMean, VelocityMaxResValue, E);
  // ��������� ������������ �������� Overeating.
  if (E <> 0) or (VelocityMaxResValue < 0.0) or (VelocityMaxResValue <= VelocityMeanValue) then Exit;
  // ��������� ��� ����� �������� ������.
  Self.VelocityMean := VelocityMeanValue;
  Self.VelocityMaxRes := VelocityMaxResValue;
  // ��� �������.
  Result := True;
end;

function TLimits.SetConsumption(const GrowthConsumptionLimits, LivingConsumptionLimit : String): Boolean;
var
  GrowthConsumptionValue, LivingConsumptionValue: Single;
  E: Integer;
begin
  Result := False;
  // ������������ �������� Starvation.
  Val(GrowthConsumptionLimits, GrowthConsumptionValue, E);
  // ��������� ������������ �������� Starvation.
  if (E <> 0) or (GrowthConsumptionValue < 0.0) then Exit;
  // ������������ �������� Overeating.
  Val(LivingConsumptionLimit, LivingConsumptionValue, E);
  // ��������� ������������ �������� Overeating.
  if (E <> 0) or (LivingConsumptionValue < 0.0)  then Exit;
  // ��������� ��� ����� �������� ������.
  Self.GrowthConsumptionLimits:= GrowthConsumptionValue;
  Self.LivingConsumptionLimit := LivingConsumptionValue;
  // ��� �������.
  Result := True;
end;


end.
