unit UModel;
interface

uses
  SysUtils, ULimits, UCoord, UField, Dialogs , StdCtrls, ULog, UCell;

  const HumusTime = 30; //�� ����� ���� � ���������� �������� ������� ������������ � ����� (��  SourceIn � Source)

type
  TRes = class   // ����� ��� �������� �������� �� ������� - ����� ������ ��� �����
    x: integer; // X
    y: integer; // Y
    s: integer; // ���������� ����� ����� � ���� ����������� � ������� ������
    growthDirection: TDirection; // ����������� �����
    searchSuccess: boolean; // ���������� ������ (���� �� ������ ������ ��������� ����� ������)
    cellType: TCellType; // ��� ������ = 0(������ ������������ ��� "���������" ������ = 3)
    haveNeighbors: boolean; // ������� ������� (��������� ��������)
    cellFamilyID: integer; // id ����� ������
    cellClanId: integer;
  end;

  TResArray = array of TRes;
  TDirectionArray = array of TDirection;
  TProbabilityArray = array of Single;

  // ������.
  TModel = class
  //�����
  private
    FField: TField;               // ����
    FLimits: TLimits;             // ������� ��������
    FStep: Integer;               // ������� ��� ������������� (�� 0)
    Ratio: Single;                // ����������� ���������� � �����������
    FClearMemAfterJump: Boolean;  // ���� True, �� ������ � ����������� ����� ��������� ����� ������
    growthClan : array of boolean;    // ������ ������� � ����� ����� � ������ �����
    growthClan1: array of boolean;    // ������ ������� � ����� ����� � ������ ����� �� ���������� ����
    clans  : array of integer;    // ������ ������ �� ������ ������ (����)
    LogForm: TLogForm;                //������ �� ��������� ������ ����� ��� �����������
    FirstMotherCell: TRes;            // ������ ����������� ������ �� ����
    FirstMotherCellBranch: TRes;      // ��������� ������ ��� ������
    isTorEnabled: boolean;            // ���������� �������� �� ���
     //  �������� � ����� ������� ���� �� ���������������
    stasisAge: integer;               // �������, ����� ������ ��������� ���� �����������
    

{
 ������ �����: ��� ������ ����������� � �����-�� �����. ��� �������� ������ ��������� �������
 famlyId - ����� �����, ���������� ������ � ������� clans,  ������ ��������� ������ ���������
 ����� �����. ����� ������� clans - ��� ������� �������� growthClan � growthClan1,
 ���� �������� ������� � ����� ����� ������. �.�. ���� ������ ������, �� ��� ������ �������
 "true" � ������ growthClan �� �������, ������ ����� � clans[famlyID]. ������ ����� ������
 ����� ��������� �� ���� ������� ������� growthClan, �.�. ���� ������� ������ �����.
 ���� ������ � �������� ����� ������ � ������� � ������� ������ �����, �� ��� ������
 ���������� clans[famlyId] ��� ��� ������ �� ���� ��������. �.�. ��������� �� ����� � ���� ����.
 � ������ ������� ����� ����� ������ growthClan ���������� � growthClan1, � ����� �����������
 ��������� "false".
 ��� ���� ��� ��� �����: ���� � ����� ��� ��������� ��� �����, �� ���� �������� ��� ������ �����
 ������ ������, �� ����������� �������� growthClan1[clans[famlyId]]. ���� ��� "false", �� ������
 � ������� ��� �� ������ ����� ����� �� ������� �������� ����-����. ������ ���� ����� ��� ���������
 ����� � ���� ������ � ��� ����������� ����� ��� �������� ������ �������. � ������ �������.
 ����� ����� �����, � �� ������ �����? �������� ��������, ��� � ����� ����������� ���������
 ������ � ��� ���� ���� ����� ����������� ������� � ���� � �� ����� ������������ ��� �����,
 � ������ ���������� �����. �.�. ��� ����� � ����� ����� ����, �� ������� ����� �� �����
 �� ���� � �������. ����� ���� ��  �� ������ ���� ����� ������������� ��� ���� � ���������
 � ���� ����� �������������� ������ ������ �����, �� ����� ������ ����������� �����������
 �������������� �����������.
 ���������� ������ ��������� ������������ ����� ���� ����������� ������ �������� �������
 clans.

 � ��� ����� ���� �� ������������ ������ ���������� � ���������� ���������� :)

}
  growthNodes: array of boolean; // ������� ������ ����� ��� ��������� ������.
  growthNodes1: array of boolean;

  FamilyCount : integer;  // ���������� ����� ������ �� ����
  NodesCount : integer;  // ����� ��������� ����� - ����. ��� ��������� �����.
  ClansCount: integer;
  private
    //��������� ������� �� ������� ����� �������� ������ �� ���������� ������� ������;
    procedure TransResources(X: Integer; Y: Integer);
    //  ����������, ������� ��� ������ ���� ��� �������� ������ grow_good2
    function NumberRepit_of_Velocity_Cycl(X, Y: Integer):integer;
    procedure Log(line: String);  //���������� ���������� � ��� ��� ����
    // �������� ������ (X,Y). �������� Direction - ����������� ����� ��� ������.
    procedure OccupyCell(X, Y: Integer; Direction: TDirection);
    // �������� �� ������ �������, ����������� ������ �� ����� �� ���� ���. ���� ����������
    // �������� � ������ ����� ������ ��� ����� �������� FLimits.StarvationDeath, �� �������
    // ������ False. ���� �������� ������ (���������� ��� �����), �� ������� ������ True.
    function ConsumeLivingResources(X, Y: Integer): Boolean;
    // �������� �� ������ �������, ����������� �� ���� ����� � ��� ������.
    procedure ConsumeGrowthResources(X, Y: Integer);
    // ������� ����� ������ (������ ��� ������ 1-�� ����).
    procedure FirstGrow(X, Y: Integer);
    // ������� ����� ������ (��������). 2
//    procedure GrowGood(X, Y: Integer);
    procedure GrowGood_2(X, Y: Integer);
    // ��������� ������ ������ �� ���� ���
    procedure decResources(X, Y: Integer);
    //��� ����������  � ������ �� �� �������
    procedure decResources1(X: Integer; Y: Integer);
    // ������� ����������� �� ������ �1 �� �2 (�������� ������ !!!)
    function getDirect(C1, C2: TCoord): TDirection;
    // �������� ����������� ����� ��� ������
    function getGrowDirect(X, Y: integer): TDirection;
    // ����������� ������ �� ��������� � �������
    // ������� ��, ���� � ������ chk ��� ������ �������� ��� � ������� �����
    // ��������� � ���������� ������� ��� chk0, �.�. ������ �� ������� ����������� �������� ���
    function checkNeighbors(growingCell, startCell: TCoord): boolean;
    // ������� � ���� ���� ������ �������� �����
    procedure takeIntoClan(C: TCoord; cl: integer); // C - ������-���������, cl - �� ����
    // ������� ���������� ����������� ������ (�������)
    function getMatherCoord(c: TCoord): TCoord;
    // ������ ��������-������ ������� ��� �����
    function getGrow(X: Integer; Y: Integer): TDirection;
    // ������� ������ ����������� (3, 5 ��� 8 ������ ������ �� ���� ��������� ����������� ����� ���� ������)
    procedure  NewDirectArea(X, Y: Integer; var na, ko :integer);
    function  getNewFamlyId(): integer;   // ������� ����� id ����� ������
    function  getNewNodeId(): integer;      // ������� ����� �������� ��� ������� �����
    function  getNewClanId(): integer;      // ������� ����� �������� ��� �����
    function getCountCrowingDirection(source: Single; limits: TLimits):integer;
    procedure TryCreateNewFamily(var R: TRes; X, Y: integer);
    procedure ColonizeCell(X, Y: integer; d: TDirection);
    procedure SetFirstMotherCellBranch(coord: TCoord);
    procedure GrowAllCells;
    procedure DeathCells;
    procedure AnalizeModelCells;
    function ApicalCellRatio(var Ap, Cen: integer):single;
    procedure PrepareClans;
    procedure PrepareGrowthNodes;
    procedure GetGrowSuccess(var result:TRes; CellToMove:TRes);
    function CreateCellToMove(coordToMove:TCoord; growthDirection: TDirection; FamilyId:integer):TRes;
    function ChooseDirectionToMove(X, Y: integer; FoundDirectionsToMove: TDirectionArray):TDirection;
    function CalculateSumResources(X, Y: integer; directionArray: TDirectionArray): Extended;
    function MakePossibleDirectionsToMove(currentCellCoord: TCoord; na, ko: integer):TDirectionArray;
    procedure addCellMeanSubstrat(X, Y: integer);
    function TryGrowInSameDirection(X, Y: integer):TDirection;
    procedure ChangeCellType(X, Y: integer);
    function CountCellByTypeBefore(X, Y: integer; var lastCellTypeCoord: TCoord; findCellType: TCellType; anyCellType: boolean):integer;  overload;
    function CountCellByTypeBefore(X, Y: integer; var lastCellTypeCoord: TCoord; findCellType: TCellType):integer;  overload;
    function CountCellBefore(X, Y: integer; var lastCellTypeCoord: TCoord):integer;
    procedure MarkClan(start, finish: TCoord; clanId: integer);

    //function isGrowingPossible(PossibleCoordToMove, currentCellCoord: TCoord): boolean;
  public

    m_transp_summ_step: single;
    //energi_centre : single;
    all_substrat: single; //����� ���������� ��������� ����� ������� ������  ������
    mean_substrat_value: single;
    modelCellsCount: integer; //���������� ������ � ������
    // �����������.
    constructor Create;               overload;
    constructor Create(Log: TLogForm);  overload;
    // ����������.
    destructor Destroy; override;
    // ������� ���� ���������� �������.
    procedure CreateField(Width, Height: Integer);
    // ���������� ������ � �������� ���������.
    procedure Reset;
    // �������� Count ������ � ��������� ������ ����. ����������
    // True, ���� ���� �� ���� ��������� ������ ���� �������.
    function Seed(Count: Integer): Boolean; overload;
    // �������� ��������� ������. ���������� True, ���� ������
    // ���� ��������, ��� False, ���� ������ ��� ���� ������.
    function Seed(X, Y: Integer): Boolean; overload;
    procedure Mean_Substrat(X, Y: Integer);

    // ��������� ���� ��� �������������.
    procedure DoStep(NoTor: Boolean);

    function getClan(i: integer): integer;
    // ��������� ���������� ��������� ���������� � �������� ����
    procedure GetRandomCoordinate(var X, Y: integer);
    procedure setStasisAge(stasisAge: integer);
    function isGrowingPossible(PossibleCoordToMove, currentCellCoord: TCoord): boolean;
    function isStasis(X, Y: integer): boolean;
  public
    // ����.
    property Field: TField read FField;
    // ������� ��������.
    property Limits: TLimits read FLimits;
    // ������� ��� ������������� (�� 0).
    property Step: Integer read FStep;
    // True, ���� ������ � ����������� ����� ��������� ����� ������.
    property ClearMemAfterJump: Boolean read FClearMemAfterJump write FClearMemAfterJump;
  end;



  
implementation

uses FMain;

//----------------------------------------------------------------------------------------------------------------------
constructor TModel.Create;
begin
  FField := nil;
  // ������� ��������� ���� 5x5.
  CreateField(5, 5);
  // ���������� ���������.
  Reset;
  FirstMotherCell := TRes.Create;
  FirstMotherCellBranch:= TRes.Create;
  FirstMotherCell.x := -1;
  FirstMotherCell.y := -1;
  FirstMotherCellBranch.X:= -1 ;
  FirstMotherCellBranch.Y:= -1;
  setStasisAge(2000000);
  LogForm := nil;
end;

constructor TModel.Create(Log: TLogForm);
begin
  Create;
  self.LogForm := Log;
end;

procedure TModel.Log(line: String);
begin
  if LogForm <> nil then begin
    LogForm.AddLog(line);
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
destructor TModel.Destroy;
begin
  // ���������� ����.
  FreeAndNil(FField);
end;

//������ � ������ �������� ������ ��������� ���������
procedure TModel.setStasisAge(stasisAge: integer);
begin
  self.stasisAge := stasisAge;
end;

//��������� �������� �� ������ � ������������� ������������ ���������
function TModel.isStasis(X, Y: integer): boolean; 
begin
  result := false;
  if not FField.IsInside(X, Y) then begin
    exit;
  end;

  result := (FStep - FField[X, Y].StepBirth) > stasisAge;
end;
// -----
function TModel.getClan(i: integer): integer;
begin
  if (Length(clans) <= 0) or (Length(clans) >= i) then begin
    result := -1;
  end else begin
    result := clans[i];
  end;
end;

//-------

function TModel.checkNeighbors(growingCell: TCoord; startCell: TCoord): boolean;
var
  x, y,k : integer;
  checkedCell : TCoord;
begin
k:=0;
  // ������� ��� �������� ������
  //growingCell := FField.Tor(x, y, isTorEnabled);
  for y := growingCell.Y-1 to growingCell.Y+1 do begin
    for x := growingCell.X-1 to growingCell.X+1 do begin


      checkedCell := FField.Tor(x, y, isTorEnabled);
      // �� ������ ������ �� ������� ���� ���� ������� �� ������ ������� ��������
      if not FField.IsInside(checkedCell) then begin
        continue;
      end;
      if not FField.IsInside(growingCell) then begin
        continue;
      end;

      if growingCell.isNotEquals(checkedCell) and startCell.isNotEquals(checkedCell)  then begin
        //  ����� ������, ��������� � ���������� �������
          
        if FField.hasDifferentNode(growingCell, checkedCell) then begin
          result := false;  // ���� ������
          exit;
        end;
      end;
    end;
  end;
  result := true; // ��� ������� - ����������� ������ �������� � ���� ������.
end;

procedure TModel.takeIntoClan(C: TCoord; cl: Integer);
begin
//
end;


//----------------------------------------------------------------------------------------------------------------------
procedure TModel.ConsumeGrowthResources(X, Y: Integer);
begin
{  // ���������, ��� � ��������� ������ ���� ����.
  if FField[X, Y].Exists then begin
    // ��������� �� "�����/������"
    case FField[X, Y].CellType of
      SIMPLE_CELL:                 // ������� ������
          decResources(X, Y);
      MOTHER_CELL:                 // ����������� ������
          decResources(X, Y);
      DAUGHTER_CELL:
          begin
            if (FField[X, Y].Age > 3) then begin    //���� ������� ������ 3 ����� (4 � �����)
              if  (FField[X, Y].Source <= Flimits.StarvationDeath) then begin
                decResources1(X, Y);  // ���� ��������� �� ���� ����. �� ��� ������ �������� ������, �������� �� ������
              end;
              if  (FField[X, Y].Source > Flimits.StarvationDeath) then begin
                decResources(X,Y);
              end;
            end else begin             // �������� �� �����������  (����������)? FField[X, Y].Age <= 10
              decResources(X,Y);
              transResources(X,Y);
            end;
          end;
      JUMPING_CELL:
          decResources(X,Y);      // �� ���� ������� ����, ���� � "���������"
    end;
  end;
  }
end;

//------------
procedure TModel.decResources(X, Y: Integer);
var
  Resources, lambda, a: Single;
begin
{    // �������� ������� �� ������.
  if (FField[X, Y].Age > 2) then begin
    lambda := 7E-5               //�����������
  end else begin
    Lambda := 0.023605263 ;      //����������
  end;

  a := FField[X, Y].Source * Lambda;
  if a < FLimits.GrowthConsumptionLimits then begin
    a := FLimits.GrowthConsumptionLimits;   //�� ������,  ��� "�� ����"
  end;

  Resources := FField[X, Y].Source - a;
  FField[X, Y].SourceIn := FField[X, Y].SourceIn + a;

  if Resources < 0.0 then begin
    Resources := 0.0;
  end;
  // ���������� ��������� ������� � ������.
  FField[X, Y].Source := Resources;
  }
end;


procedure TModel.decResources1(X: Integer; Y: Integer);  //������ ���� �������� ??? ����������� ��������� ������
var
  delta : Single;
  X1, Y1 : integer;     //���������� ������
begin
{    delta :=FLimits.LivingConsumptionLimit; // FLimits.GrowthConsumptionLimits;
    //���������� ������
    X1 := FField[X, Y].RelatedCellCoord.X;
    Y1 := FField[X, Y].RelatedCellCoord.Y;

  // �������� ������� �� �������. ������ � ��������� �� �������. ������   (�����)
  if FField[X1, Y1].SourceIn < delta then begin
    FField[X, Y].SourceIn := FField[X, Y].SourceIn + FField[X1, Y1].SourceIn; // ��� �������� ����
    FField[X1, Y1].SourceIn := 0.0;
  end else begin
    FField[X1, Y1].SourceIn := FField[X1, Y1].SourceIn - delta;
  end;
  }
end;


procedure TModel.TransResources(X, Y: Integer);
const
  Vmax = 1.1;
  Km = 6.2E-5;
var
  Resources, Cons: Single;
begin
{  Cons := FField[X, Y].Source * Vmax * Ratio /6*(Km + FField[X, Y].Source); // ��������� ������
  Resources := FField[X, Y].Source - Cons;
  m_transp_summ_step := Cons+ m_transp_summ_step;
  if Resources < 0.0 then begin
    Resources := 0.0;
  end;
  // ���������� ��������� � �������� � ����������� ������.
  FField[X, Y].Source := Resources;
  FField[FField[X, Y].RelatedCellCoord.X, FField[X, Y].RelatedCellCoord.Y].SourceIn := FField[FField[X, Y].RelatedCellCoord.X, FField[X, Y].RelatedCellCoord.Y].SourceIn + Cons;
  }
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.ConsumeLivingResources(X, Y: Integer): Boolean;
var
  lim: Single;
  c1, c2: Tcoord;
  TempRes:single;
begin
{  lim := FLimits.StarvationDeath;  //��� �������� ������
  //ConsumeGrowthResources(X,Y);   //��������� �� ���� ????
  //FField[X, Y].Source := FField[X, Y].Source - FLimits.LivingConsumptionLimit;   //��������� �� �����
  c1 := coord(X, Y);
  c2 := coord(FField[X, Y].RelatedCellCoord.X, FField[X, Y].RelatedCellCoord.Y);

  FField[X, Y].Source := FField[X, Y].Source - FLimits.LivingConsumptionLimit;   //��������� �� �����  �������� � 9 ��� ������

  if FField[X, Y].CellType = DAUGHTER_CELL then begin // � �������� ��� �� ��� ������...
    Result := FField[FField[X, Y].RelatedCellCoord.X, FField[X, Y].RelatedCellCoord.Y].Source > lim;   // ���������� True, ���� � ������ �������� ������ ��������.
  end else begin
    Result := FField[X, Y].Source > lim;    // ���������� True, ���� �������� ������ ��������.
  end;
  }
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.CreateField(Width, Height: Integer);
begin
  // ���������� ����.
  FreeAndNil(FField);
  // � ������� ����� ������� �������.
  FField := TField.Create(Width, Height);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.DoStep(NoTor: Boolean);
var
  X, Y, X1,Y1: Integer;
begin
  isTorEnabled := not NoTor;

//  PrepareClans;   // ������� ������� ��� ������� � ������ ����� ������ ����� ������
//  PrepareGrowthNodes;   // ������� ������� ����� ����� (�����)

  //������ ������ �� ������� ����: ������� ���� ���������� ������
  AnalizeModelCells;

  // ������� ��� ������ ���� - ����������� �����.
  GrowAllCells;


  // ������� ��� ������ ���� - ����������� ������ ��� ��������.
  //DeathCells;

  // ����������� ������� �����.
  Inc(FStep);
end;

procedure TModel.PrepareClans;
var
  x: integer;
begin
  // ������� ������� ��� ������� � ������ ����� ������ ����� ������
  SetLength(clans, FamilyCount + 1); // ������ ������ ������ �� ���������� �����, � ������ ��� ������������ ��������� ������
  SetLength(growthClan, FamilyCount + 1);
  SetLength(growthClan1, FamilyCount + 1);
  if FStep = 0 then begin  // ����� � ���� �1
    growthClan[1] := true;
    growthClan1[1] := false;
    clans[1] := 1;
  end else begin
    for x := 0 to FamilyCount do begin
      growthClan1[x]:=growthClan[x];
      growthClan[x]:=false;
    end;
  end;
end;

procedure TModel.PrepareGrowthNodes;
var
  X, Y, X1, Y1: integer;
begin
  // ������� ����� ����� (�����)
  SetLength(growthNodes, NodesCount + 1);
  SetLength(growthNodes1, NodesCount + 1);
  if step = 0 then begin  // node �1
    m_transp_summ_step := 0; //���������� ����������� �� ������� � ������� ������� ���� ����� ����
    mean_substrat_value := 0.001;
    all_substrat := 1;
    growthNodes[1] := true;
    growthNodes1[1] := false;
  end else begin
    all_substrat := 0;
    X1 := FField.Width - 1;
    Y1 := FField.Height-1;

    //���������� �������: �, Y - �� ���������������� � ��� ��� ����������� �� �������!!!
    if (X = X1) and (Y = Y1) then begin
      Mean_Substrat(X, Y);
    end;

    for x := 0 to NodesCount do begin
      growthNodes1[x] := growthNodes[x];
      growthNodes[x] := false;
    end;
  end;

end;

procedure TModel.AnalizeModelCells;
var
  Ap, Cen: integer;
begin
  ApicalCellRatio(Ap, Cen);
  modelCellsCount := Cen + Ap;
end;

function TModel.ApicalCellRatio(var Ap, Cen: integer):single;
var
  X, Y,k: integer;
begin
  // ������� ��� ������ ���� ��� �������� ���� ���������� ������ � ���������� ���� ������,
  //����� �� c ������� ���� ������ ��������� modelCellsCount, ������� ����� �����������
  //��� ������� ������������ ����������
  Cen := 0;
  Ap := 0;
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      
      if FField[X, Y].Exists then begin
        if (FField[X, Y].Age > 2) then
          inc(Cen)

        else
          inc(Ap);
      end;
    end;
  end;

  Result := 0;

  if (Cen + Ap) > 0 then begin //���� ��� ����������� ���������� ������ �� ���� (����� ������� ����� �� ������������)
    Result := Ap /(Cen + Ap);
  end;

end;

procedure TModel.GrowAllCells;
var
  X, Y,k: integer;
begin
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      if FField[X, Y].Exists and (not FField[X, Y].alreadyGrow) then begin      // ���������, ��� � ������ ���� ����.
        
        GrowGood_2(X, Y);
      end;
    end;
  end;
end;

function TModel.NumberRepit_of_Velocity_Cycl(X, Y: Integer):integer;
// ���������� ����� �������� ����� �������� � ��������� growgood_2
var

checkedCell : TCoord;
begin

  checkedCell:=FField.Tor(x, y, isTorEnabled);
 

  x:=checkedCell.X;
  y:=checkedCell.y;

  Result:=0;
  
  if (FField[X, Y].Source>1.2*limits.StarvationDeath) and
    (FField[X, Y].Source<0.9*limits.OvereatingToxic)
    then Result:=2;
  if (FField[X, Y].Source>0.9*limits.OvereatingToxic) and
    (FField[X, Y].Source<1.2*limits.OvereatingToxic)
    then Result:=1;
end;

procedure TModel.GrowGood_2(X, Y: Integer);
var
  DirCount,k1: Integer; //������� ��������� -  ������� ��� � ������ ������� ����� �� ����� ������
  K,z: Integer; //���������� ��� ����� �� ��������� �������� ���������
  ForVelocityCount: Integer;//������� ��� ����� ��������- �� ����� �� ������ ����� � ���� �������
  R: TDirection; //���������� ��� ��������� ����������� �����
  //UsesCoord:Tcoord;
  //������� ��� ����� ��������- �� ����� �� ������ ����� � ���� �������
  UsesCoords, InitialCoords: Tcoord;      //  ������������ ������ ����� ������� growgood 2


begin
  // ������ �� ������ �� ���� ��������.
  if FField[X, Y].StepBirth = FStep then begin
    Exit;
  end;

  z:=1;

  DirCount := getCountCrowingDirection(FField.getSource(X, Y), FLimits);

  // ���� �����: 1 ��� 2 ��������.
  for K := 1 to DirCount do begin
    // ���� ����������� ��� �����
    R := getGrowDirect(X, Y);
    if R = dNone then begin      //���� ����� �� �������, �� ������� � ��������� ��������
      Continue;
    end;
    ForVelocityCount:=0;
    InitialCoords := FField.Tor(Coord(X, Y), isTorEnabled);
    UsesCoords := FField.Tor(Coord(X, Y), isTorEnabled);
//    TryCreateNewFamily(R, X, Y);
    for ForVelocityCount := 0 to NumberRepit_of_Velocity_Cycl(FField.Tor(Coord(X, Y), isTorEnabled).X,FField.Tor(Coord(X, Y), isTorEnabled).y) do
        begin
          InitialCoords.copyFrom(UsesCoords);
          UsesCoords := InitialCoords + coord(R);
          UsesCoords:=FField.Tor(UsesCoords.x, UsesCoords.y, isTorEnabled);
          z:=1;

  
          //�������� �� ���� � ��� ������?
          if isGrowingPossible(UsesCoords, InitialCoords) then begin
             ColonizeCell(InitialCoords.X, InitialCoords.y, R);
          end else begin
            break;
          end;
        end;
    end;
end;

function TModel.isGrowingPossible(PossibleCoordToMove, currentCellCoord: TCoord): boolean;
//  ����� �� ����� � ��� ������? ���� ����� ������� � ��� �� �������
var

  R1:boolean;
begin
//���������� �� �������� ��� �����?
  result := (FField.getSource(PossibleCoordToMove) > Limits.StarvationDeath)
//��� �� � ������, ���� ������, ��� ������ ������?
        and (not FField.isExists(PossibleCoordToMove))
        //��� �� � ������, ���� ������, �������?
        and checkNeighbors(PossibleCoordToMove, currentCellCoord);
end;

//���������� ��������� ���������� ����������� ����� � ����������� �� ����������
//�������� � ������ � ����������� ������.
function TModel.getCountCrowingDirection(source: Single; limits: TLimits):integer;
begin
  Result := 1;

  // ���� �������� ���������� ��� ���������, �� ����� ����� � ���� ������������.
  if (source > limits.OvereatingToxic) and (source < limits.FullOvereatingToxic) then begin
    Result := 2;
  end else if source >= limits.FullOvereatingToxic then begin
    Result := 3;
  end;
end;

function TModel.getGrowDirect(X: Integer; Y: Integer): TDirection;
var
  growDirectionResult: TDirection;
  neig: boolean;
  currentCoord, foundCoord: TCoord;

begin

  result := dNone;

  if FField[X, Y].StepBirth >= FStep then begin
    exit;
  end;

  
  neig := false;
  // 1. ������� ����� � �� �� �������
  result := TryGrowInSameDirection(X, Y);  //
  currentCoord := coord(X, Y);
  foundCoord := currentCoord + Coord(result);
 
  neig := checkNeighbors(foundCoord, currentCoord);

  if (result = dNone) or (not neig) then begin
      result := getGrow(X, Y);    // 2. ������� ����� � ������� ����������� �� �������. ���� ���� ���-��
  end;
  
end;

function TModel.TryGrowInSameDirection(X, Y: integer):TDirection;
const
  // ����������� ����� � ���� � �� �� �������.
  Probability: array[0..3] of Single = (0.426, 0.625, 0.899, 0.976);
var
  xx,k: Integer;
  C: TCoord;
begin
  xx := FField[X, Y].DickLength-1;
  if xx > 3 then begin
    xx:=3;
  end;

  Result := dNone;

  // 1 - ������ � ������� ����������� � ������������ ������������ � ������ �����
  // 2 - ��� ������ ��������, ��� ���������, ��� ����� ������������
  if (Fstep mod 2=0) then// ����� ������ (��������) �� �������� ����������� �����
  begin
    C := Coord(X,Y)+Coord(1,1);
    C := FField.Tor(C, isTorEnabled); // ������ � ������������ �� ��������� ���� "���������" �� ������ ������� ����
    if FField.IsInside(C) then begin
      //Result := ;  // ���� ������ ��� ������, �� ����� ������ ������
      if not FField[C.X, C.Y].Exists then begin
          
          Result := FField[X, Y].Direction;
          end;
    end;
  end else
  begin
     C := Coord(X,Y)+Coord(1,3);
     C := FField.Tor(C, isTorEnabled); // ������ � ������������ �� ��������� ���� "���������" �� ������ ������� ����
    if FField.IsInside(C) then begin
      //Result := ;  // ���� ������ ��� ������, �� ����� ������ ������
      if not FField[C.X, C.Y].Exists then begin
        Result := FField[X, Y].Direction;
      end;
    end;
  end;


end;

function TModel.getGrow(X: Integer; Y: Integer): TDirection;
var
  na, ko, L: integer;
  FoundDirectionsToMove: TDirectionArray;
 // CellToMove: TRes;
 //directionToMove: TDirection;
begin
  result := dNone;
  // ����� ������ ���� ����� �����
  NewDirectArea(X, Y, na, ko); //  na,ko   ��� ����������� �� ������� �������
  FoundDirectionsToMove := MakePossibleDirectionsToMove(Coord(X, Y), na, ko);


  if Length(FoundDirectionsToMove) = 0 then begin    // ��������� ���, ���� ����� ������
    exit;
  end;

  result := ChooseDirectionToMove(X, Y, FoundDirectionsToMove);

//  GetGrowSuccess(result, directionToMove);
  
end;

function TModel.MakePossibleDirectionsToMove(currentCellCoord: TCoord; na, ko: integer):TDirectionArray;
var
//  FoundCellsToMove: TResArray;
  PossibleCoordToMove: TCoord;
  i, n: integer;
  lastIndex: integer;
  dn: TDirection;
begin
  // �������� ������ ��������� ����������� �����
  SetLength(result, 0);

  for i := na to ko do begin
    if i > 8 then begin //�������� �� ������� �������, ����� 8 ���� 1
      n := i-8
    end else begin
      n := i;
    end;
    dn := TDirection(n);
    PossibleCoordToMove := currentCellCoord + Coord(dn);
    PossibleCoordToMove := FField.Tor(PossibleCoordToMove, isTorEnabled); // ���� ������ �� ��������� ����...
    if not FField.IsInside(PossibleCoordToMove) then begin
      Continue;
    end;
    if (FField.getSource(PossibleCoordToMove) > Limits.StarvationDeath)
        and (not FField.isExists(PossibleCoordToMove))
        and checkNeighbors(PossibleCoordToMove, currentCellCoord) then begin
      //����� ���� ��������� ������� (�������� ������  � ������������ �������:����������)
      lastIndex := Length(result);
      SetLength(result, lastIndex + 1);
      result[lastIndex] := dn;
    end;
  end;
end;

//�������� ������ �������� �� �������� ��������� ������
function TModel.ChooseDirectionToMove(X, Y: integer; FoundDirectionsToMove: TDirectionArray):TDirection;
var
  i, L: integer;
  rr, totalResource: Extended;    //���������� �������� � ��������� ��� �������� �������
  newCoord: TCoord;
begin
  result := dNone;

  totalResource := CalculateSumResources(X, Y, FoundDirectionsToMove);

  rr := 1*totalResource;   // �������� ��������� �������� �� ������� ���������, �� ������ � �������� ��������� ����� ������ �����������
  L := -1;
  for i := 0 to Length(FoundDirectionsToMove) - 1 do begin
    newCoord := Coord(X, Y) + Coord(FoundDirectionsToMove[i]);
    newCoord := FField.Tor(newCoord, isTorEnabled);
    if not FField.IsInside(newCoord) then begin
      continue;
    end;
    totalResource := totalResource - FField[newCoord.X, newCoord.Y].Source;
    if totalResource <= rr then begin
      L := i;
      break;
    end;
  end;

  if L > -1 then begin
    result := FoundDirectionsToMove[L];
  end;

end;


//������������ ����� ���������� �������� �� ���� �������� ������� ��� ���������� ��������
function TModel.CalculateSumResources(X, Y: integer; directionArray: TDirectionArray): Extended;
var
  i: integer;
  c: TCoord;
begin
  result := 0;
  for i := 0 to Length(directionArray)-1 do begin
    c := Coord(X, Y) + Coord(directionArray[i]);
    c := FField.Tor(c, isTorEnabled);
    if FField.IsInside(c) then begin
      result := result + FField.getSource(c.X, c.Y);
    end;
  end;
end;

procedure  TModel.NewDirectArea(X,Y: Integer; var na, ko :integer);
var
  d, d1, d2 :integer;
begin
  d1 := Integer(FField[X, Y].Direction);                //������ ������ ���� �����������
  d2 := Integer(FField.getPreviousDirection(X, Y));     // ����������� �������� ������
  if d2 = 0 then begin
    d2 := d1;
  end;
  if (d1=0) or (d2=0) then begin
    na := 1;
    ko := 8;
    exit;
  end;  // �� ������� �����

  // ������� ��� - ���������
  if (d1 mod 2) = 1 then begin
    na := d1 - 1;
    ko := d1 + 1;
  end else begin
    // � ��������� �� ������
    if (d2 mod 2) =1 then begin
      na := d2 - 1;
      ko := d2 + 1;
    end else begin
      //  �� ������
      if d2 = d1 then begin
        na := d1 - 2;
        ko := d1 + 2;
      end else begin
        // ��� ������ �����
        d := Round((d1 + d2) / 2);
        na := d - 1;
        ko := d + 1;
        if (d = 5) and ((d1 = 2) or (d1 = 8)) then begin // ��� ������ �����  (������ ���� 10 ��� ��� 2)
          na := 8;
          ko := 10;
        end;
      end;
    end;
  end;
end;




procedure TModel.DeathCells;
var
  X, Y: integer;
begin
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      // ���������, ��� � ������ ���� ����.
      if (FField[X, Y].Exists) and (FField[X, Y].StepBirth < FStep) then begin      // ���� ��� �� ���, �� ������� ���� ��� ������
        if mean_substrat_value < 0.00000004  then begin//���������� ����� ���� 0,0004, �� �� �� ���� �������� ����������. �������� ���������� �������
          if (FField[X,Y].Age > Fstep/100) then begin
            FField.Kill(X, Y, FStep);
          end;
        end;

        if not ConsumeLivingResources(X, Y) then begin                  // �������� ������� �� �����.  ??? �� ���� ������-��???
          FField.Kill(X, Y, FStep);           // ������� � ������ �����������, �� ���� ������ ���������.
        end;
      end else begin
        if (FStep-FField[X, Y].StepDeath=HumusTime) and (FStep>HumusTime) then begin
          FField.Humus(X, Y);
        end;
      end;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.FirstGrow(X, Y: Integer);
var
  Dir, D: TDirection;  // ����������� �����
  C: TCoord;
begin
{  // ������ �� ������ �� ���� ��������.
  if FField[X, Y].StepBirth = FStep then begin
    Exit;
  end;

  // �������� ����������� ����� �� ��������.
  Dir := getGrowDirect(X, Y).growthDirection;   //��������� ����� ������ (�� ������)
  // ���� ����������� �� ����������, �� �� �� ������.
  if Dir = dNone then begin
    Exit;
  end;

  // ��������� ���������� ������.
  C := Coord(X, Y) + Coord(Dir);
  C := FField.Tor(C, isTorEnabled);
  if FField.IsInside(C.X, C.Y) then begin
    // ������ � ��� ������.
    OccupyCell(C.X, C.Y, Dir);
    FField[C.X, C.Y].FamilyId := FField[X, Y].FamilyId;
    FField[C.X, C.Y].isDivided := false;
    FField[C.X, C.Y].Node := FField[X, Y].Node;
      // �������� ������� �� ����.
    ConsumeGrowthResources(C.X, C.Y);
  end;

  // ���� �������� �����, �������� ������ ����������� �����.
  if FField[X, Y].Source > FLimits.OvereatingToxic then begin
    // ������ 8 ������� ������ ������.
    for K := 1 to 8 do begin
      // �������� �������� �����������.
      D := TDirection(Random(Ord(High(TDirection)) + 1));
      // ���������, ��� ��� ����������� ���������� �� 1-��.
      if D <> Dir then begin
        // ������� ����������.
        C := Coord(X, Y) + Coord(D);
        C := FField.Tor(C, isTorEnabled);
        if FField.IsInside(C.X, C.Y) then begin
          // ���������, ��� � ���� ������ ���������� ��������.
          if (FField[C.X, C.Y].Source > FLimits.StarvationDeath) then begin
            // ������ � ��� ������.
            OccupyCell(C.X, C.Y, D);
            FField[C.X, C.Y].FamilyId := FField[X, Y].FamilyId;
            FField[C.X, C.Y].Node := getNewNodeId();
            FField[C.X, C.Y].isDivided := false;
            FField[X, Y].isDivided := true;

              // �������� ������� �� ����.
            ConsumeGrowthResources(C.X, C.Y);
            // ����������� �������.
            Break;
          end;
        end;
      end;
    end;
  end;
  }
end;

// ���������� ��������� � �� ��� ����� ����
{
procedure TModel.GrowGood(X, Y: Integer);
var
  DirCount, K: Integer;
  R: TRes;
begin
  // ������ �� ������ �� ���� ��������.
  if FField[X, Y].StepBirth = FStep then begin
    Exit;
  end;

  DirCount := getCountCrowingDirection(FField.getSource(X, Y), FLimits);

  // ���� �����: 1 ��� 2 ��������.
  for K := 1 to DirCount do begin
    // ���� ������ ��� �����
    R := getGrowDirect(X, Y);
    if not R.searchSuccess then begin      //���� ����� �� �������, �� ������� � ��������� ��������
      Continue;
    end;

//    TryCreateNewFamily(R, X, Y);

    //�������� ������.
    ColonizeCell(R, X, Y);

    // �������� ������� �� ��������
    ConsumeGrowthResources(R.X, R.Y);

  end;
end;
}


procedure TModel.TryCreateNewFamily(var R: TRes; X, Y: integer);
begin
{  if (FFIeld[X, Y].CellType = DAUGHTER_CELL) and (FField[R.X, R.Y].Source > FLimits.StarvationDeath)  then begin  // ??? �������� ��������   ��� ����������� �����
    // �������� ������ ����������� �� ������� � ��������� � ������� ����� �����
    R.cellFamilyID := getNewFamlyId();   // ����� �����
    SetLength(growthClan, R.cellFamilyID + 1);
    // ������� ����� ���� ��� ���� �����
    SetLength(clans, R.cellFamilyID + 1);
    clans[R.cellFamilyID] := R.cellFamilyID;  // ������������� id ����� � ����� ���������.
    //TODO: ��������� ������ � ���� ������!!!!!!!!!!!!!!!
//    R.cellType := DAUGHTER_CELL;
    //!!!!!!!!!!!!!!!
  end;
  }
end;

procedure TModel.ColonizeCell(X, Y: integer; d: TDirection);
var
  checkedCell,newCoord: TCoord;
  newCell: TCell;
begin
  checkedCell:=FField.Tor(x, y, isTorEnabled);
  x:=checkedCell.X;
  y:=checkedCell.y;

  FField[X, Y].alreadyGrow := true;
  newCoord := Coord(X, Y) + Coord(d);

  newCoord := FField.Tor(newCoord, isTorEnabled);
  newCell := FField[newCoord.X, newCoord.Y];
  newCell.Birth(FStep);

  newCell.Direction := d;
  newCell.RelatedCellCoord := Coord(X, Y);
  if FField[X, Y].Direction = d then begin
    newCell.DickLength := FField[X, Y].DickLength + 1;
  end;

  newCell.FamilyId := FField[X, Y].FamilyId;

  if not FFIeld[X, Y].isDivided then begin // ������ ������ ��������� ������ �����, ��������� �������� �����.
    newCell.Node := FField[X, Y].Node;
    FField[X, Y].isDivided := true;
  end else begin
    newCell.Node := getNewNodeId();
    setLength(growthNodes, NodesCount + 1);
  end;

  newCell.CellType := SIMPLE_CELL;
  ChangeCellType(newCoord.X, newCoord.Y); 
                                    
end;

procedure TModel.SetFirstMotherCellBranch(coord: TCoord);
begin
  if FirstMotherCellBranch.X = -1 then begin
    FirstMotherCellBranch.X := coord.X;
    FirstMotherCellBranch.Y := coord.Y;
    log('���������� ������ ����������� ���������� ������ ' + FloatToStr(FirstMotherCell.x) + ', ' + FloatToStr(FirstMotherCell.y)) ;
    log('�������� ������� ����� ����������� ���������� ������' + FloatToStr(Ffield.getSource(FirstMotherCell.x, FirstMotherCell.y)));
  end;
end;

//------------
//TODO: ��������� ���� ���������� ������ ���������� MOTHER_CELL
function TModel.getMatherCoord(c: TCoord): TCoord;
var
  previousCellCoord: TCoord;
begin
  { �������� ������� "�" - ��� � ���� ���������� ������.
    ����� ������� � ���, ����� ���������, ���� ���� �� ����. � ���� ������
    ������� ������� �� ���� (�������)
    ������� ���������� �������� ���� ����, ����� ����������� ����� ����� �����
  }
  if FField[C.X, C.Y].CellType <> DAUGHTER_CELL then begin
    result := C;
    previousCellCoord := FField[C.X, C.Y].RelatedCellCoord;
    if FField[previousCellCoord.X, previousCellCoord.Y].CellType <> MOTHER_CELL then begin
      FField[C.x, C.Y].CellType := MOTHER_CELL; // ������� �������. �������� ��� ����� ������ � ������� �������
    end;
//    FField[C.x, C.Y].CellType := MOTHER_CELL;
  end else begin
    result:=getMatherCoord(FField[C.X, C.Y].RelatedCellCoord);
  end;
end;



       
procedure TModel.ChangeCellType(X, Y: integer);
var
  relatedCellCoord, lastSimpleCellCoord, lastCellCoord: TCoord;
  relatedCell: TCell;
  cell: TCell;
  count: integer;
  clanId: integer;
begin
  cell := FField[X, Y]; 
  cell.cellType := SIMPLE_CELL;                    //�� ��������� ���������� SIMPLE_CELL
  relatedCellCoord := FField[X, Y].RelatedCellCoord;
  relatedCell := FFIeld[relatedCellCoord.X, relatedCellCoord.Y];

  if relatedCell.CellType = DAUGHTER_CELL then begin
    cell.CellType := DAUGHTER_CELL;
    if relatedCell.ClanId > -1 then begin
      cell.ClanId := relatedCell.ClanId;
    end;
    exit;
  end;
  
  if (FField[X, Y].Source >= FLimits.FullOvereatingToxic) then begin
    cell.CellType := DAUGHTER_CELL;
    count := CountCellBefore(X, Y, lastCellCoord);
    if count > 1 then begin
      FField[lastCellCoord.X, lastCellCoord.Y].CellType := MOTHER_CELL;
      MarkClan(Coord(x, y), lastCellCoord, getNewClanId);
    end else begin
      if count = 1 then begin
        if FField.IsInside(lastCellCoord.X, lastCellCoord.Y) then begin
          relatedCellCoord := FField[lastCellCoord.X, lastCellCoord.Y].RelatedCellCoord;
          relatedCell := FFIeld[relatedCellCoord.X, relatedCellCoord.Y];
          clanId := relatedCell.ClanId;
        end else begin
          clanId := getNewClanId;
        end;
        MarkClan(Coord(x, y), lastCellCoord, clanId);
      end;
    end;
    exit;
  end;


  if CountCellByTypeBefore(x, y, lastSimpleCellCoord, SIMPLE_CELL) = FMain.MainForm.getMatherLenght then begin
    FField[lastSimpleCellCoord.X, lastSimpleCellCoord.Y].CellType := MOTHER_CELL;
    cell.cellType := DAUGHTER_CELL;
    MarkClan(Coord(x, y), lastSimpleCellCoord, getNewClanId);
  end;
    
end;

procedure TModel.MarkClan(start, finish: TCoord; clanId: integer);
begin
  if not (FField.isInside(start) and FField.IsInside(finish)) then begin
    exit;
  end;
  FField[start.X, start.Y].ClanId := clanId;
  while start.isNotEquals(finish) and FField.IsInside(start) do begin
    start := FField[start.X, start.Y].RelatedCellCoord;
    if FField.IsInside(start) then begin
      FField[start.X, start.Y].ClanId := clanId;
    end;
    
  end;
end;

function TModel.CountCellByTypeBefore(X, Y: integer; var lastCellTypeCoord: TCoord;
                                      findCellType: TCellType):integer;
begin
  result := CountCellByTypeBefore(X, Y, lastCellTypeCoord, findCellType, false);
end;

function TModel.CountCellBefore(X, Y: integer; var lastCellTypeCoord: TCoord):integer;
begin
  result := CountCellByTypeBefore(X, Y, lastCellTypeCoord, ANY_CELL, true);
end;

function TModel.CountCellByTypeBefore(X, Y: integer; var lastCellTypeCoord: TCoord; 
                                      findCellType: TCellType;
                                      anyCellType: boolean):integer;
var  
  tmp: TCoord;
  next, current, relatedCell: TCell;

begin

  result := 0;
  lastCellTypeCoord := Coord(-1, -1);

  if (FField[X, Y].CellType = findCellType) or anyCellType then begin
    result := 1;
  end;

  repeat
    if (FField[X, Y].CellType = findCellType) or anyCellType then begin
      lastCellTypeCoord := Coord(X, Y);    
    end;

    current := FField[X, Y];
    next := current;
    tmp := FField[X, Y].RelatedCellCoord;   
    

    if FField.isInside(tmp) then begin
      if FField.isExists(tmp) then begin
        if FField[tmp.X, tmp.Y].ClanId = -1 then begin
          next := FField[tmp.X, tmp.Y];
          inc(result);
        end;
        X := tmp.X;
        Y := tmp.Y;
      end;
      
    end;
    
  until next = current;
end;


        

function TModel.CreateCellToMove(coordToMove:TCoord; growthDirection: TDirection; FamilyId:integer):TRes;
begin
   result := TRes.Create();
   result.x := coordToMove.X;
   result.y := coordToMove.Y;
   result.growthDirection := growthDirection;
   result.s := 0;
   result.haveNeighbors := true; //checkNeighbors(coord(PossibleCoordToMove.x, PossibleCoordToMove.y), coord(x,y));
   result.cellFamilyID := FamilyId;
end;


//��������� ��� ����������� ����������� ������ ������ ��� �������� (��� �������� ������)
procedure TModel.GetGrowSuccess(var result:TRes; CellToMove:TRes);

begin

  result.searchSuccess := true;
  result.x := CellToMove.X;
  result.y := CellToMove.Y;
   
  result.growthDirection := CellToMove.growthDirection;
  result.s := CellToMove.s;
  result.cellFamilyID := CellToMove.cellFamilyID;
  result.haveNeighbors := CellToMove.haveNeighbors;
end;

// -----------

//------------
function TModel.getDirect(C1: TCoord; C2: TCoord): TDirection;
var
  index: integer;
begin
   result := TDirection.dNone; // �� ��������� warnings
   // �������� �������� ��������� � ������ ��� ������ �����������
   index := (C2.X - C1.X + 1)*3 + (C2.Y - C1.Y + 1);
   case index of
    0: result := TDirection.dUpLeft;
    1: result := TDirection.dLeft;
    2: result := TDirection.dDownLeft;
    3: result := TDirection.dUp;
    4: result := TDirection.dNone;
    5: result := TDirection.dDown;
    6: result := TDirection.dUpRight;
    7: result := TDirection.dRight;
    8: result := TDirection.dDownRight;
   end
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.OccupyCell(X, Y: Integer; Direction: TDirection);

begin

  // �������� ������.

  FField[X, Y].Exists := True;
  FField[X, Y].StepBirth := FStep;
  FField[X, Y].Age := 0;
//  FField[X, Y].CellType := SIMPLE_CELL;
  // ���������� ����������� �����.
  FField[X, Y].Direction := Direction;
  FField[X, Y].DickLength := 0;
  FField[X, Y].RelatedCellCoord := Coord(-1, -1);
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TModel.Reset;
begin
  // �������� �� ���������.
  FLimits.StarvationDeath := 0.2;
  FLimits.OvereatingToxic := 0.6;
  FamilyCount := 0;
  NodesCount := 0;
  // ���������� ������� �����.
  FStep := 0;
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.Seed(Count: Integer): Boolean;
var
  Cells: array of TCoord;
  X, Y, L, i, j: Integer;
  C: TCoord;
begin
  // ������� ��� ���������� ������
  // � ��������� �� � ������ Cells.
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      // ������ ������ ���� ��������� � � ��� ������ ���� ���������� ��������.
      if not FField[X, Y].Exists and (FField[X, Y].Source > FLimits.StarvationDeath) then begin
        L := Length(Cells);
        SetLength(Cells, L + 1);
        Cells[L] := Coord(X, Y);
      end;
    end;
  end;

  // ������������ ������ Cells.     ??????
  for i := 0 to High(Cells) - 2 do begin
    j := i + 1 + Random(High(Cells) - i);
    C := Cells[i];
    Cells[i] := Cells[j];
    Cells[j] := C;
  end;

  // ���� ���������� ������ ������������, �� ������������
  // �������� ���������� ���������� ������ Count.
  if Count > Length(Cells) then begin
    Count := Length(Cells);
  end;

  // �������� ����� � ��������� ������.
  for i := 0 to Count - 1 do begin
    Seed(Cells[I].X, Cells[I].Y);
  end;

  // ������� ������ True, ���� ����
  // �� 1 ������ ����� �������.
  Result := (Count > 0);
end;

//----------------------------------------------------------------------------------------------------------------------
function TModel.Seed(X, Y: Integer): Boolean;
begin
  Result := False;
  // ���������, ��� ������ ��������� � �������� ����
  if FField.IsInside(X, Y) then begin
    // �������� ������: ��� ���������� ������, ��� ������ ������ ��������� ��������� ��
    // ���������� ���� �������������, � �� ������� ��� ���� ������ ���� ����� 1 ����.
    with FField[X, Y] do begin
      Exists := True;
      Direction := dNone;
      DickLength := 0;
      StepBirth := FStep - 1;
      StepDeath := 0;
      Age := 1;
      FamilyId := getNewFamlyId();
      Node := getNewNodeId();
      CellType := SIMPLE_CELL;
    end;
    Result := True;
  end;
end;

//TODO: ��� ������ ������ ��� �������?
procedure TModel.Mean_Substrat(X, Y: Integer);
begin
  {mean_substrat_value := 0;
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      addCellMeanSubstrat(X, Y);      //���� ������� � ��������� ���������
    end;
  end;
  if modelCellsCount > 0 then begin  
    mean_substrat_value := all_substrat / modelCellsCount;
  end;}
end;

//TODO: ��������� �������
procedure TModel.addCellMeanSubstrat(X, Y: integer);
var
  i, j:integer;
  C: TCoord;
begin
  {//�������� ��� ��������� �������
  if not FField[X, Y].Exists then begin
    for i := X-1 to X+1 do begin
      for j := Y-1 to Y+1 do begin
        if Y>2 then
  Y:=y;
        C := FField.Tor(i, j, isTorEnabled);
        if not FField.IsInside(C) then begin
          continue;
        end;
        if (FField[C.X, C.Y].exists) then begin
          All_substrat := all_substrat + FField[C.X, C.Y].Source;
        end;
            
      end;
    end;
  end;}
end;

//--------------------
function TModel.getNewFamlyId(): integer;   // ��� ��������
begin
  inc(FamilyCount);
  result := FamilyCount;
end;

//----------------
function TModel.getNewNodeId(): integer;
begin
  inc(NodesCount);
  result := NodesCount;
end;

function TModel.getNewClanId(): integer;
begin
  inc(ClansCount);
  result := ClansCount;
end;

procedure TModel.GetRandomCoordinate(var X, Y: integer);
begin
  if (Field.Width > 0) and (Field.Height > 0) then begin
    X := random(Field.Width);
    Y := random(Field.Height);
  end else begin
    X := -1;
    Y := -1;
  end;
end;

end.
