unit UCell;

interface

uses
  UCoord;

{const
  SIMPLE_CELL = 0;
  MOTHER_CELL = 1;
  DAUGHTER_CELL = 2;
  JUMPING_CELL = 3;
                  }

type
  TCellType = ( ANY_CELL = -2,
                EMPTY_CELL = -1,
                SIMPLE_CELL = 0,
                MOTHER_CELL = 1,
                DAUGHTER_CELL = 2,
                JUMPING_CELL = 3);


// ������ ����.
  TCell = class
  public

    Exists: Boolean;                       // ���� �� ���� � ������
    Age: Integer;                          // ������� �����
    Source: Single;                        // ������ � ������
    SourceIn: Single;                      // ������ � ������
    Direction: TDirection;  // ����������� ����� ����� (1 ���������, 2 ���������� � ����� �� ��������� ���)
    DickLength: Integer;                   // ���-�� �����, � ������� ������� ���� ��� � ����� �����������
    StepBirth, StepDeath: Integer;         // ���, �� ������� ������ ���� ������� � ��� ������
    CellType: TCellType;                   // ��� ������ 0,1,2 - �������, �����������, ��������
    RelatedCellCoord: TCoord;              // ���������� ��������� ������, ���� ��� ����
    FamilyId  : integer;                   // id ����� - ������ ������ ������ �������������
    Node  : integer;                       // id �����. ������������ �� ������� ����, ��� ���� ���������
    isDivided: boolean;                     // ��, ���� ������ ������� ��� ��������
    ClanId: integer;
    alreadyGrow: boolean;

    function MotherType: string;           //������??? ���������� �� ��� ������ ���������� ��������� ������������� ��� ����������� ������
    procedure Kill(step: Integer);         //"�������" ���� � ������
    procedure toHumus;                     // ���������� ������� �� ������� �����
    procedure Birth(step: integer);        //"��������" �����
    constructor Create();
  end;


implementation

constructor TCell.Create();
begin
  inherited;
  CellType := EMPTY_CELL;
  FamilyId := -1;
  ClanId := -1;
  alreadyGrow := false;
end;

//���������� ��������� ������������� ��� ����������� ������
function TCell.MotherType: string;
begin
  case CellType of
    SIMPLE_CELL: result:='������� ������';
    MOTHER_CELL: result:='����������� ������';
    DAUGHTER_CELL: result:='�������� ������';
    JUMPING_CELL: result:='��������� ������';
    else result:='����������� ������';
  end;
end;

//"�������" ���� � ������
procedure TCell.Kill(step: Integer);
begin
  if Exists then  begin
    Exists := False;
    Age := 0;
    Direction := dNone;
    CellType := EMPTY_CELL;
    RelatedCellCoord := Coord(-1, -1);
    DickLength := 0;
    StepBirth := 0;
    StepDeath := step;
    FamilyId := -1;
    ClanId := -1;
    alreadyGrow := false;
  end;
end;

// ���������� ������� �� ������� �����
procedure TCell.toHumus;
begin
  if not Exists then begin
    // ������� ��������
    Source := Source + SourceIn;
    SourceIn := 0.0;
    StepDeath := 0;
  end;
end;

//"��������" �����
procedure TCell.Birth(step: integer);
begin
  Exists := True;
  StepBirth := step;
  Age := 0;
  isDivided := false;
end;

end.
